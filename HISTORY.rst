Changelog
---------

0.3.0
^^^^^
**release date:** *not released*

* Better naming convention (no backward incompatibility, only in core, not in api)
* new doc
* python3 support

0.2.0
^^^^^
**release date:** 2014-02-04

* Change the name of the program: theTVDB API becomes thetvdb_api
* Change the name of some functions : get_series_infos() become get_series() and get_episode_infos() become get_episodes()
* set_cache() has more options: second argument is optional and tells how long the cache must be hold

0.1.0
^^^^^
**release date:** 2014-02-02

* Initial release