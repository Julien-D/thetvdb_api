Actors
======

.. module:: thetvdb_api.api
  :noindex:
  
.. autofunction:: get_actors
   :noindex:

The :func:`~thetvdb_api.api.get_actors` function returns a dictionary with one sub-dictionary for each actor of the series. These sub-dictionaries have the following keys:

* :mod:`Image`: The location of the artwork.
* :mod:`Name`: The actors real name.
* :mod:`Role`: The name of the actors character in the series.
* :mod:`SortOrder`: An integer from 0-3. 1 being the most important actor on the show and 3 being the third most important actor. 0 means they have no special sort order. Duplicates of 1-3 aren't suppose to be allowed but currently are so the field isn't perfect but can still be used for basic sorting. 