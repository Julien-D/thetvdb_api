API
===

.. module:: thetvdb_api.api

.. autofunction:: set_cache

.. autofunction:: set_series

.. autofunction:: set_language

.. autofunction:: get_series

For more details about the output of the function, see :doc:`series`.

.. autofunction:: get_episode

For more details about the output of the function, see :doc:`episode`.

.. autofunction:: get_banners

For more details about the output of the function, see :doc:`banners`.

.. autofunction:: get_actors

For more details about the output of the function, see :doc:`actors`.