Banners
=======

.. module:: thetvdb_api.api
  :noindex:
  
.. autofunction:: get_banners
   :noindex:

The :func:`~thetvdb_api.api.get_banners` function returns a dictionary with one sub-dictionary for each banner of the series. These sub-dictionaries have the following keys:

* :mod:`BannerPath`: the location of the artwork.
* :mod:`ThumbnailPath`: used exactly the same way as BannerPath, only shows if BannerType is fanart.
* :mod:`VignettePath`: used exactly the same way as BannerPath, only shows if BannerType is fanart.
* :mod:`BannerType`: this can be poster, fanart, series or season.
* :mod:`BannerType2`: for series banners it can be text, graphical, or blank. For season banners it can be season or seasonwide. For fanart it can be 1280x720 or 1920x1080. For poster it will always be 680x1000. Blank banners will leave the title and show logo off the banner. Text banners will show the series name as plain text in an Arial font. Graphical banners will show the series name in the show's official font or will display the actual logo for the show. Season banners are the standard DVD cover format while wide season banners will be the same dimensions as the series banners.
* :mod:`Language`: some banners list the series name in a foreign language. The language abbreviation will be listed here.
* :mod:`Season`: if the banner is for a specific season, that season number will be listed here.
* :mod:`Rating`: returns either null or a decimal with four decimal places. The rating the banner currently has on the site.
* :mod:`RatingCount`: always returns an unsigned integer. Number of people who have rated the image.
* :mod:`SeriesName`: this can be true or false. Only shows if BannerType is fanart. Indicates if the seriesname is included in the image or not.
* :mod:`Colors`: returns either null or three RGB colors in decimal format and pipe delimited. These are colors the artist picked that go well with the image. In order they are Light Accent Color, Dark Accent Color and Neutral Midtone Color. It's meant to be used if you want to write something over the image, it gives you a good idea of what colors may work and show up well. Only shows if BannerType is fanart. 