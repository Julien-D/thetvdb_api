Episode
=======

.. module:: thetvdb_api.api
  :noindex:
  
.. autofunction:: get_episode
   :noindex:
   
The :func:`~thetvdb_api.api.get_episode` function returns a dictionary with the following keys:

* :mod:`id`: an unsigned integer assigned by theTVDB.com to the episode. Cannot be null.
* :mod:`Combined_episodenumber`: an unsigned integer or decimal. Cannot be null. This returns the value of DVD_episodenumber if that field is not null. Otherwise it returns the value from EpisodeNumber. The field can be used as a simple way of prioritizing DVD order over aired order in your program. In general it's best to avoid using this field as you can accomplish the same task locally and have more control if you use the DVD_episodenumber and EpisodeNumber fields separately.
* :mod:`Combined_season`: an unsigned integer or decimal. Cannot be null. This returns the value of DVD_season if that field is not null. Otherwise it returns the value from SeasonNumber. The field can be used as a simple way of prioritizing DVD order over aired order in your program. In general it's best to avoid using this field as you can accomplish the same task locally and have more control if you use the DVD_season and SeasonNumber fields separately.
* :mod:`DVD_episodenumber`: a decimal with one decimal and can be used to join episodes together. Can be null, usually used to join episodes that aired as two episodes but were released on DVD as a single episode. If you see an episode 1.1 and 1.2 that means both records should be combined to make episode 1. Cartoons are also known to combine up to 9 episodes together, for example Animaniacs season two.
* :mod:`DVD_season`: an unsigned integer indicating the season the episode was in according to the DVD release. Usually is the same as EpisodeNumber but can be different.
* :mod:`Director`: a pipe delimited string of directors in plain text. Can be null.
* :mod:`DirectorStarsList`: same as Director but in Python List format. Can be null.
* :mod:`EpImgFlag`: an unsigned integer from 1-6.

  1. :mod:`4:3` - Indicates an image is a proper 4:3 (1.31 to 1.35) aspect ratio. 
  2. :mod:`16:9` - Indicates an image is a proper 16:9 (1.739 to 1.818) aspect ratio. 
  3. :mod:`Invalid Aspect Ratio` - Indicates anything not in a 4:3 or 16:9 ratio. We don't bother listing any other non standard ratios. 
  4. :mod:`Image too Small` - Just means the image is smaller then 300x170. 
  5. :mod:`Black Bars` - Indicates there are black bars along one or all four sides of the image. 
  6. :mod:`Improper Action Shot` - Could mean a number of things, usually used when someone uploads a promotional picture that isn't actually from that episode but does refrence the episode, it could also mean it's a credit shot or that there is writting all over it. It's rarely used since most times an image would just be outright deleted if it falls in this category. 
  
* :mod:`EpisodeName`: a string containing the episode name in the language requested. Will return the English name if no translation is available in the language requested.
* :mod:`EpisodeNumber`: an unsigned integer representing the episode number in its season according to the aired order. Cannot be null.
* :mod:`FirstAired`: a string containing the date the series first aired in plain text using the format "YYYY-MM-DD". Can be null.
* :mod:`GuestStars`: a pipe delimited string of guest stars in plain text. Can be null.
* :mod:`GuestStarsList`: same as GuestStars but in Python List format. Can be null.
* :mod:`IMDB_ID`: an alphanumeric string containing the IMDB ID for the series. Can be null.
* :mod:`Language`: a two character string indicating the language in accordance with ISO-639-1. Cannot be null.
* :mod:`Overview`: a string containing the overview in the language requested. Will return the English overview if no translation is available in the language requested. Can be null.
* :mod:`ProductionCode`: an alphanumeric string. Can be null.
* :mod:`Rating`: The average rating users of theTVDB.com have rated the series out of 10, rounded to 1 decimal place. Can be null.
* :mod:`RatingCount`: an unsigned integer representing the number of users who have rated the series. Can be null.
* :mod:`SeasonNumber`: an unsigned integer representing the season number for the episode according to the aired order. Cannot be null.
* :mod:`Writer`: a pipe delimited string of writers in plain text. Can be null.
* :mod:`WriterList`: same as Writer but in Python List format. Can be null.
* :mod:`absolute_number`: an unsigned integer. Can be null. Indicates the absolute episode number and completely ignores seasons. In others words a series with 20 episodes per season will have Season 3 episode 10 listed as 50. The field is mostly used with cartoons and anime series as they may have ambiguous seasons making it easier to use this field.
* :mod:`airsafter_season`: an unsigned integer indicating the season number this episode comes after. This field is only available for special episodes. Can be null.
* :mod:`airsbefore_episode`: an unsigned integer indicating the episode number this special episode airs before. Must be used in conjunction with airsbefore_season, do not with airsafter_season. This field is only available for special episodes. Can be null.
* :mod:`airsbefore_season`: an unsigned integer indicating the season number this special episode airs before. Should be used in conjunction with airsbefore_episode for exact placement. This field is only available for special episodes. Can be null.
* :mod:`filename`: The location of the episode image. Can be null.
* :mod:`lastupdated`: Unix time stamp indicating the last time any changes were made to the episode. Can be null.
* :mod:`seasonid`: an unsigned integer assigned by theTVDB.com to the season. Cannot be null.
* :mod:`seriesid`: an unsigned integer assigned by theTVDB.com to the series. It does not change and will always represent the same series. Cannot be null.
* :mod:`thumb_added`: a string containing the time the episode image was added to theTVDB.com in the format "YYYY-MM-DD HH:MM:SS" based on a 24 hour clock. Can be null.
* :mod:`thumb_height`: an unsigned integer that represents the height of the episode image in pixels. Can be null
* :mod:`thumb_width`: an unsigned integer that represents the width of the episode image in pixels. Can be null 