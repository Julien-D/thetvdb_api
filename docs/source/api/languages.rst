Available languages
===================

Available languages are the following:

* English: :mod:`en`
* Svenska: :mod:`sv`
* Norsk: :mod:`no`
* Dansk: :mod:`da`
* Suomeksi: :mod:`fi`
* Nederlands: :mod:`nl`
* Deutsch: :mod:`de`
* Italiano: :mod:`it`
* Español: :mod:`es`
* Français: :mod:`fr`
* Polski: :mod:`pl`
* Magyar: :mod:`hu`
* Greek: :mod:`el`
* Turkish: :mod:`tr`
* Russian: :mod:`ru`
* Hebrew: :mod:`he`
* Japanese: :mod:`ja`
* Portuguese: :mod:`pt`
* Chinese: :mod:`zh`
* Czech: :mod:`cs`
* Slovenian: :mod:`sl`
* Croatian: :mod:`hr`
* Korean: :mod:`ko`