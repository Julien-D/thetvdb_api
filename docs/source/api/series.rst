Series
======

.. module:: thetvdb_api.api
  :noindex:
  
.. autofunction:: get_series
   :noindex:
   
The :func:`~thetvdb_api.api.get_series` function returns a dictionary with the following keys (some are only available if you have specified a language in :func:`~thetvdb_api.api.set_series` or with the :func:`~thetvdb_api.api.set_language` function):

* :mod:`seriesid`: an unsigned integer that the unique identifier of the series. Cannot be null.
* :mod:`id`: same as *:mod:`seriesid**. Cannot be null.
* :mod:`language`: a two digit string indicating the language. Cannot be null.
* :mod:`SeriesName`: a string containing the series name in the language you requested. Will return the English name if no translation is found in the language requested. Can be null if the name isn't known in the requested language or English.
* :mod:`AliasNames`: a pipe "|" delimited list of alias names if the series has any other names in that language. Can be null.
* :mod:`SeriesNameList`: a list (a Python list) of all the names of the series (SeriesNames and AliasNames). Cannot be null.
* :mod:`banner`: the relative path to the highest rated banner for this series. Can be null.
* :mod:`Overview`: a string containing the overview in the language requested. Will return the English overview if no translation is available in the language requested. Can be null.
* :mod:`FirstAired`: the first aired date for the series in the "YYYY-MM-DD" format. Can be null.
* :mod:`IMDB_ID`: the IMDB id for the series if known. Can be null.
* :mod:`zap2it_id`: the zap2it ID if known. Can be null.
* :mod:`Network`: the Network name if known. Can be null.

The parameters below are only available if a language has been specified:

* :mod:`Actors`: a pipe delimited string of actors in plain text. Begins and ends with a pipe even if no actors are listed. Cannot be null.
* :mod:`ActorsList`: same as Actors but in Python List format. Cannot be null.
* :mod:`Airs_DayOfWeek`: the full name in English for the day of the week the series airs in plain text. Can be null.
* :mod:`Airs_Time`: a string indicating the time of day the series airs on its original network. Format "HH:MM AM/PM". Can be null.
* :mod:`ContentRating`: the rating given to the series based on the US rating system. Can be null or a 4-5 character string.
* :mod:`Genre`: a pipe delimited list of genres in plain text. Begins and ends with a | but may also be null.
* :mod:`GenreList`: same as Genre but in Python List format. Cannot be null.
* :mod:`Rating`: the average rating users of theTVDB.com have rated the series out of 10, rounded to 1 decimal place. Can be null.
* :mod:`RatingCount`: an unsigned integer representing the number of users who have rated the series. Can be null.
* :mod:`Runtime`: an unsigned integer representing the runtime of the series in minutes. Can be null.
* :mod:`Status`: a string containing either "Ended" or "Continuing". Can be null.
* :mod:`added`: a string containing the date/time the series was added to theTVDB.com in the format "YYYY-MM-DD HH:MM:SS" based on a 24 hour clock. Is null for older series.
* :mod:`addedBy`: an unsigned integer. The ID of the theTVDB user who added the series to our database. Is null for older series.
* :mod:`fanart`: the highest voted fanart for the requested series. Can be null.
* :mod:`lastupdated`: Unix time stamp indicating the last time any changes were made to the series. Can be null.
* :mod:`posters`: the highest voted poster for the requested series. Can be null. 