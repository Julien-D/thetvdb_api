.. thetvdb_api documentation master file, created by
   sphinx-quickstart on Sat Feb  8 12:44:09 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

thetvdb_api
===========
Release v\ |release|

thetvdb_api is a Python API for theTVDB.com. It is very simple to use and has a lot of features.

Features
--------
This API allows you to access all the information of theTVDB.com including:

* Informations about a tv show (such as first aired, network...)
* Informations about an episode (such as director, writer...)
* All the banners linked to a show
* All the actors of a show

Usage
-----
First of all, you will need an API key from theTVDB.com. You can easily get one from `this registration page <http://thetvdb.com/?tab=apiregister>`_.

Get the show informations
^^^^^^^^^^^^^^^^^^^^^^^^^
There are 2 ways to create theTVDB object depending on the following situations:

* you know in what language you want to retrieve the informations of the show. Be aware that all the shows are not available in all languages
* you don't know the language

You know the language
"""""""""""""""""""""
::

  import thetvdb_api

  thetvdb_api.set_cache('some folder') #set the cache folder
  series = thetvdb_api.set_series('some show name', 'your tvdb api key', 'language')

  series = thetvdb_api.get_series(series)

For example::

  import thetvdb_api

  thetvdb_api.set_cache('~/.cache')

  series = thetvdb_api.set_series('The Office (US)', 'XXXXXXXXXXXXXXXX', 'en')
  series_infos = thetvdb_api.get_series(series)

If :mod:`set_cache()` is not called, the result of the request won't be cached.
:mod:`set_cache()` accepts a second optional argument which is the time to hold cache in seconds (default is 21600)

Language must be one of the languages supported by theTVDB.com. See :doc:`api/languages`.

See :func:`~thetvdb_api.api.get_series` for more information.

You don't know the language
"""""""""""""""""""""""""""
::

  import thetvdb_api

  thetvdb_api.set_cache('some folder') #set the cache folder
  series = thetvdb_api.set_series('some show name', 'your tvdb api key')

  series_infos = thetvdb_api.get_series(series )

For example::

  import thetvdb_api

  thetvdb_api.set_cache('~/.cache')
  series = thetvdb_api.set_series('The Office (US)', 'XXXXXXXXXXXXXXXX')

  series_infos = thetvdb_api.get_series(series)

See :func:`~thetvdb_api.api.get_series` for more information.

You can later specify a language and get some more show information by doing::

  thetvdb_api.set_language(series, 'en')
  series_infos = thetvdb_api.get_series(series)

Get the episode informations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
A language must have been set through the constructor or the :mod:`set_language()` function. Otherwise, English will be chosen but it can lead to unexpected output.
::

  episode_infos = thetvdb_api.get_episode(series, 'season_number', 'episode_number')

For example::

  episode_infos = thetvdb_api.get_episode(series, 3, 9)

:samp:`get_episode()` accepts a third argument 'language' if you want to specify a language different from the one defined in the constructor or the :samp:`set_language()` function.

See :func:`~thetvdb_api.api.get_episode` for more information.

Get the banners
^^^^^^^^^^^^^^^
::

  banners = thetvdb_api.get_banners(series)

See :func:`~thetvdb_api.api.get_banners` for more information.

Get the actors
^^^^^^^^^^^^^^
::

  actors= thetvdb_api.get_actors(series)

See :func:`~thetvdb_api.api.get_actors` for more information.

License
-------
GNU General Public License v3 (GPLv3)

API Documentation
-----------------
If you are looking for information on a specific function, class or method, this part of the documentation is for you.

.. toctree::
  :maxdepth: 2

  api/api
  api/series.rst
  api/episode.rst
  api/banners.rst
  api/actors.rst
  api/languages.rst
  
.. include:: ../../HISTORY.rst