# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from api import set_cache, set_series, set_language, get_series, get_episode, get_banners, get_actors
from theTVDBError import *

__name__ = 'thetvdb_api'
__version__ = '0.3.1'
__author__ = 'Julien-D'
__copyright__ = 'Copyright 2014 '+__author__